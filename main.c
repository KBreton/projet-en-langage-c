#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"
int main()
{
    printf("Bonjour! :) c'est parti !! \n");
    bloc1();
    bloc2();
}
 void bloc1(){
    printf("______________________\nLancement du BLOC 1\n");
    FILE* document_source = NULL;
    FILE* document_cible_lignes = NULL;
    document_source = fopen("document_source.csv", "r");
	document_cible_lignes = fopen("document-cible-lignes.txt", "w");
	char bool_croise;
	char bool_ptvirgule;
	char caractere=0;
	char chiffre[10]={'0','1','2','3','4','5','6','7','8','9'};
    if (document_source != NULL)
    {
        while ((caractere=fgetc(document_source)) != EOF)
        {

            for(int num=0;num<10;num++){ // Si un chiffre avant un ; --> VRAI
                if(caractere==chiffre[num]){
                    bool_croise=1;
                }
                if(caractere==';' && !bool_croise){
                    bool_ptvirgule=1;
                }
            }
            if(!bool_ptvirgule){
                if(bool_croise){
                    if(document_cible_lignes != NULL){
                            fputc(caractere, document_cible_lignes);
                            //printf("caractere ecrit\n");
                    }
                }
            }
            if(caractere=='\n'){
                bool_croise=0;
                bool_ptvirgule=0;
            }
        }
    }
    printf("Fin du BLOC1\n____________________");
    fclose(document_source);
    fclose(document_cible_lignes);
 }

void bloc2(){
    printf("\nLancement du BLOC2\n_______________________\n");
    FILE* document_source = NULL;
    FILE* document_cible = NULL;
    document_source = fopen("document-cible-lignes.txt", "r");
    document_cible = fopen("document-cible-codep.txt", "w");
    int codep=1000000;
    char bool_sautdeligne;
    char caractere=0;
    char ligne_tab[300];
    char *pointeur;
    pointeur = ligne_tab;
    if (document_source != NULL){
            // stocker la ligne dans ligne_tab , quand on croise \n: �crire codep puis copier le contenu de ligne_tab
        while ((caractere=fgetc(document_source)) != EOF){

            *pointeur=caractere;

            if(caractere=='\n'){
                *(pointeur+1)='\0';
                fprintf(document_cible,"%d:%s",codep,ligne_tab);
                codep++;
                pointeur=ligne_tab;
            }
            else {
                pointeur++;
            }
        }


    }
    fclose(document_source);
    fclose(document_cible);
}
